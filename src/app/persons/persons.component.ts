import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { PersonsService } from './persons.service';

@Component({
  selector: 'app-persons',
  templateUrl: './persons.component.html'
})
export class PersonsComponent implements OnInit, OnDestroy {
  personSubscribe: Subscription;
  personList: string[];
  isFetching: boolean;

  constructor(private prsService: PersonsService) {}

  ngOnInit(): void {
    this.personSubscribe = this.prsService.personsChange.subscribe(persons => {
      this.isFetching = false;
      this.personList = persons;
    });
    this.isFetching = true;
    this.prsService.fetchPersons();
  }

  ngOnDestroy(): void {
    this.personSubscribe.unsubscribe();
  }

  removePerson(name: string) {
    this.prsService.removePerson(name);
  }
}
