import { Component, EventEmitter, Output } from '@angular/core';
import { PersonsService } from '../persons/persons.service';

@Component({
  selector: 'app-person-input',
  templateUrl: './person-input.component.html',
  styleUrls: ['./person-input.component.scss']
})
export class PersonInputComponent {
  enteredPersonName = '';

  constructor(private prsService: PersonsService) {}

  onCreateUser() {
    this.prsService.addPerson(this.enteredPersonName);
    console.log(this.prsService.persons);
    this.enteredPersonName = '';
  }
}
